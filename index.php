<?php
ini_set('display_errors','Off');
include_once "Manager.php";
$Manager = new Manager($_POST);
$manage = $Manager->index(); 
$total = $Manager->sum();
?>


<!DOCTYPE html>


<html>
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-sale=1.0">
        </head>
        <body>
            <div>
                <a href="create.php">ADD NEW</a>
            </div>
            <table border="1">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>weekday</th>
                        <th>date</th>
                        <th>manager</th>
                        <th>quantity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($manage as $element){
                    ?>
                    <tr>
                    <td><?php echo $element->id;?></td>
                    <td><?php echo $element->weekday;?></td>
                    <td><?php echo $element->date;?></td>
                    <td><?php echo $element->manager;?></td>
                    <td><?php echo $element->quantity;?></td>
                    <td>View | Edit | Delete | Trash/Recover</td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
                 <tfoot>
                        <th colspan="3">Total</th>

                        <th><?php echo $total ?></th>
                        </tfoot>
                
            </table>
             <div><a href="#"> <span> prev  1 | 2 | 3 | 4 | 5 next </span></a></div>
            </body>
    </html>